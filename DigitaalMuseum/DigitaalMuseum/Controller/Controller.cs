﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DigitaalMuseum
{
  public class Controller
  {
    // Alle communicatie van view naar model gaat via controller
    // Controller maakt de model en view klasses aan en koppelt ze
    // Laten we van de controller een singleton maken...
    private static Controller instance = new Controller();
    private Archief archief;

    // Property
    public static Controller Instance
    {
      get { return instance; }
    }

    // Constructor
    private Controller()
    {
        archief = new Archief();
    }

    // Methodes
    public View.DefaultView getDefaultView()
    {
        View.DefaultView view = new View.DefaultView();
        archief.addObserver(view);
        return view;
    }

    public View.KunstwerkView getKunstwerkView()
    {
        View.KunstwerkView view = new View.KunstwerkView();
        archief.addObserver(view);
        return view;
    }

    public View.LijstKunstenaarsView getLijstKunstenaarsView()
    {
        View.LijstKunstenaarsView view = new View.LijstKunstenaarsView();
        archief.addObserver(view);
        return view;
    }

    public void btnZoekOpNrClicked(string input)
    {
        int nummer;
        bool isNumber = int.TryParse(input, out nummer);  //check of de inputwaarde wel numeriek is
        if (isNumber)
        {
            if (nummer >= 0 && nummer < archief.GetAantalKunstwerken())
            {
                archief.TempNummer = nummer;
                archief.DefaultViewIsVisible = false;
                archief.KunstwerkViewIsVisible = true;
                archief.LijstKunstenaarsViewIsVisible = false;
            }
            else
            {
                MessageBox.Show("Er bestaat geen kunstwerk met nummer " + nummer.ToString(), "", MessageBoxButtons.OK);
            }
        }
        else
        {
            MessageBox.Show("Foute inputwaarde! Enkel numerieke karakters zijn toegestaan.");
        }
    }

    public void btnZoekKunstenaarClicked(string input)
    {
        bool kunstenaarGevonden = false;
        foreach (Artiest artiest in archief.GetArtiesten())
        {
            if (artiest != null && input.Trim() != "")
            {
                if (artiest.GetNaam().ToUpper() == input.ToUpper())
                {
                    archief.TempArtiest = artiest;
                    kunstenaarGevonden = true;
                }
            }
        }
        if (kunstenaarGevonden)
        {
            archief.DefaultViewIsVisible = false;
            archief.KunstwerkViewIsVisible = false;
            archief.LijstKunstenaarsViewIsVisible = true;

        }
        else
        {
            MessageBox.Show("Kunstenaar niet gevonden.");
        }
    }

    public void btnHomeClicked()
    {
        archief.DefaultViewIsVisible = true;
        archief.KunstwerkViewIsVisible = false;
        archief.LijstKunstenaarsViewIsVisible = false;
    }

    public bool DefaultViewVisibility()
    {
        if (archief.DefaultViewIsVisible)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool KunstwerkViewVisibility()
    {
        if (archief.KunstwerkViewIsVisible)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool LijstKunstenaarsViewVisibility()
    {
        if (archief.LijstKunstenaarsViewIsVisible)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public string GetKunstwerkNr()
    {
        return archief.GetKunstwerk().GetNummer().ToString();
    }

    public string GetKunstwerkNaam()
    {
        return archief.GetKunstwerk().GetNaam();
    }

    public string GetKunstenaarNaam()
    {
        return archief.GetKunstwerk().GetArtiest().GetNaam();
    }

    public string GetKunstenaarJaar()
    {
        return archief.GetKunstwerk().GetJaar();
    }

    public string GetKunstwerkUitleg()
    {
        return archief.GetKunstwerk().GetUitleg();
    }

    public Kunstwerk[] GetKunstwerkenKunstenaar()
    {
        return archief.GetKunstwerken(archief.TempArtiest);
    }

    public string GetTempKunstenaar()
    {
        return archief.TempArtiest.GetNaam();
    }

    public void ListKunstwerkenSelected(string item)
    {
        foreach (Kunstwerk kunstwerk in GetKunstwerkenKunstenaar())
        {
            if (kunstwerk != null)
            {
                if (kunstwerk.GetNaam() == item)
                {
                    archief.TempNummer = kunstwerk.GetNummer();
                    archief.DefaultViewIsVisible = false;
                    archief.KunstwerkViewIsVisible = true;
                    archief.LijstKunstenaarsViewIsVisible = false;
                }
            }

        }
    }

  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitaalMuseum
{
  public class Artiest : Observable
  {
    private string naam;
    private int geboortejaar;

    public Artiest(string setNaam, int setGeboortejaar)
    {
      naam = setNaam;
      geboortejaar = setGeboortejaar;
    }

    public string GetNaam()
    {
      return naam;
    }

    public int GetGeboortejaar()
    {
        return geboortejaar;
    }
  }
}

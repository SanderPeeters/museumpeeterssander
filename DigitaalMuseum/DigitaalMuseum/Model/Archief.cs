﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitaalMuseum
{
  public class Archief : Observable, Observer
  {
    private Kunstwerk[] kunstwerken;
    private Artiest[] artiesten;

    private bool defaultViewIsVisible = true;
    private bool kunstwerkViewIsVisible = false;
    private bool lijstKunstenaarsViewIsVisible = false;

    private int tempNummer = 0;
    private Artiest tempArtiest;

    public Archief()
    {
        kunstwerken = new Kunstwerk[9];
        artiesten = new Artiest[3];

        //Kunstwerken aanmaken
        //Kunstenaar: Sandro Botticelli
        artiesten[0]   = new Artiest("Sandro Botticelli", 1445);
        kunstwerken[0] = new Kunstwerk(0, "De geboorte van Venus", " Rond 1483 of vroeger", "Het beeldt de godin Venus die oprijst uit de zee als een volgroeide vrouw, zoals deze beschreven wordt in de Griekse mythologie. De naam van het werk is echter niet helemaal in overeenkomst met de gebeurtenis die erop staat afgebeeld, aangezien Venus volgens de legende uit het zeeschuim zou zijn geboren. Deze afbeelding toont echter haar aankomst in Cyprus, staande op een schelp.", artiesten[0]);
        kunstwerken[1] = new Kunstwerk(1, "La Primavera", "Circa 1477", "De titel betekent letterlijk 'De Lente' en is vrijwel zeker besteld als bruidsgeschenk voor een neef van Lorenzo de Medici 'Il Magnifico' en heeft vervolgens in de slaapkamer van het bruidspaar gehangen.", artiesten[0]);
        kunstwerken[2] = new Kunstwerk(2, "De geboorte Van Christus", "1445 – 1510", "Dit werk is ook bekend als de Mystieke Geboorte. Maria, de os en de ezel waken over het kindje terwijl Jozef slaapt. De drie knielende mannen links zijn de Drie Wijzen uit het Oosten, te herkennen aan hun lange gewaden. Knielend rechts de herders, met bescheidener en kortere kleding.", artiesten[0]);

        //Kunstenaar: Rembrandt Van Rijn
        artiesten[1]   = new Artiest("Rembrandt Van Rijn", 1606);
        kunstwerken[3] = new Kunstwerk(3, "De Nachtwacht", "1639 - 1642", "De compagnie van kapitein Frans Banning Cocq en luitenant Willem van Ruytenburgh maakt zich gereed om uit te marcheren. Dit werk, een schuttersstuk, werd door een compagnie uit de schuttersgilde als groepsportret besteld.", artiesten[1]);
        kunstwerken[4] = new Kunstwerk(4, "Terugkeer van de Verloren Zoon", "1668", "In dit schilderij geeft Rembrandt de parabel van de verloren zoon uit het Nieuwe Testament weer. We zien een geknielde jongeman, die omhelsd wordt door een oudere man, zijn vader. Rechts zijn drie mannen afgebeeld, die in stilte toekijken. De staande man rechts is de broer van de verloren zoon.", artiesten[1]);
        kunstwerken[5] = new Kunstwerk(5, "Jong meisje in het venster", "1651", "Wie het meisje is is onbekend. Ze werd in het verleden wel geïdentificeerd als Hendrickje Stoffels met wie Rembrandt in 1651 samenwoonde. Hendrickje was op dat moment echter 25 terwijl het meisje op het schilderij een stuk jonger is. In het midden van de 17e eeuw maakte Rembrandt meerdere schilderijen op de grens van portret en genrestuk met jonge vrouwen.", artiesten[1]);

        //Kunstenaar: Vincent Van Gogh
        artiesten[2]   = new Artiest("Vincent Van Gogh", 1853);
        kunstwerken[6] = new Kunstwerk(6, "De Sterrennacht", "1889", "Het schilderij is een nachttafereel met gele sterren boven een kleine stad met heuvels. Het is een uitzicht vanuit een denkbeeldig punt over een dorp met kerktoren en links een vlammende cipres en rechts olijfbomen tegen de heuvels op. Voor het gebruik van complementaire kleuren greep Van Gogh terug op Delacroix.", artiesten[2]);
        kunstwerken[7] = new Kunstwerk(7, "Zonnebloemen", "1888", "Van de serie zonnebloemen bestaan er drie schilderijen met vijftien zonnebloemen in een vaas en twee schilderijen met twaalf zonnebloemen in een vaas. Vincent Van Gogh begon met schilderen van de werken in het einde van de zomer van 1888 en vervolgde dit een jaar later. De schilderijen gingen naar zijn vriend Paul Gauguin als decoratie voor zijn slaapkamer. De schilderijen laten verschillende perioden van bloei van de zonnebloemen zien, van volle bloei tot verwildering.", artiesten[2]);
        kunstwerken[8] = new Kunstwerk(8, "Zelfportret", "1886", "Een zelfportret van Vincent Van Gogh, geschilderd in zijn typische postimpressionistische stijl.", artiesten[2]);


    }

    // Properties

    public bool DefaultViewIsVisible
    {
        get { return defaultViewIsVisible; }
        set
        {
            this.defaultViewIsVisible = value;
            NotifyObservers();
        }
    }

    public bool KunstwerkViewIsVisible
    {
        get { return kunstwerkViewIsVisible; }
        set
        {
            this.kunstwerkViewIsVisible = value;
            NotifyObservers();
        }
    }

    public bool LijstKunstenaarsViewIsVisible
    {
        get { return lijstKunstenaarsViewIsVisible; }
        set
        {
            this.lijstKunstenaarsViewIsVisible = value;
            NotifyObservers();
        }
    }

    public int TempNummer
    {
        get { return tempNummer; }
        set { tempNummer = value; }
    }

    public Artiest TempArtiest
    {
        get { return tempArtiest; }
        set { tempArtiest = value; }
    }

    // Methodes
    public Kunstwerk GetKunstwerk()
    {
        return this.kunstwerken[this.tempNummer];
    }

    public Artiest[] GetArtiesten()
    {
        return this.artiesten;
    }

    public Kunstwerk[] GetKunstwerken(Artiest artiest)
    {
        Kunstwerk[] kunstwerkenKunstenaar = new Kunstwerk[9];

        int nrKunstwerkGevonden = 0;
        foreach (Kunstwerk kunstwerk in this.kunstwerken)
        {
            if (kunstwerk != null)
            {
                if (kunstwerk.GetArtiest().GetNaam() == artiest.GetNaam())
                {
                    kunstwerkenKunstenaar[nrKunstwerkGevonden] = kunstwerk;
                    nrKunstwerkGevonden++;
                }
            }
        }
        return kunstwerkenKunstenaar;
    }

    public int GetAantalKunstwerken()
    {
        return kunstwerken.Length;
    }

    public void ModelChanged()
    {
        NotifyObservers();
    }
  }
}

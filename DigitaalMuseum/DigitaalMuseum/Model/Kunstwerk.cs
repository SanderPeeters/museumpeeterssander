﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitaalMuseum
{
    public class Kunstwerk : Observable
    {
        private int nummer;
        private string naam;
        private string uitleg;
        private string jaar;
        private Artiest artiest;

        public Kunstwerk(int setNummer, string setNaam, string setJaar, string setUitleg, Artiest setArtiest)
        {
            this.nummer = setNummer;
            this.naam = setNaam;
            this.jaar = setJaar;
            this.uitleg = setUitleg;
            this.artiest = setArtiest;
        }

        public int GetNummer()
        {
            return this.nummer;
        }

        public string GetUitleg()
        {
            return this.uitleg;
        }

        public Artiest GetArtiest()
        {
            return this.artiest;
        }

        public string GetNaam()
        {
            return this.naam;
        }

        public string GetJaar()
        {
            return this.jaar;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DigitaalMuseum
{
  public partial class Form1 : Form, Observer
  {
    View.DefaultView defaultView;
    View.KunstwerkView kunstwerkView;
    View.LijstKunstenaarsView lijstKunstenaarsView;

    public Form1()
    {
      InitializeComponent();
      defaultView = Controller.Instance.getDefaultView();
      defaultView.Location = new Point(0, 0);
      Controls.Add(defaultView);

      kunstwerkView = Controller.Instance.getKunstwerkView();
      kunstwerkView.Location = new Point(0, 0);
      Controls.Add(kunstwerkView);
      kunstwerkView.Visible = false;

      lijstKunstenaarsView = Controller.Instance.getLijstKunstenaarsView();
      lijstKunstenaarsView.Location = new Point(0, 0);
      Controls.Add(lijstKunstenaarsView);
      lijstKunstenaarsView.Visible = false;
    }

    public void ModelChanged()
    {
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitaalMuseum
{
    // We gebruiken een interface voor de Observer klasse: een interface is een klasse met enkel lege methodes. 
    // De overervende klasses implementeren dan die methodes.
    public interface Observer
    {
        void ModelChanged();
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DigitaalMuseum.View
{
    public partial class LijstKunstenaarsView : UserControl, Observer
    {
        public LijstKunstenaarsView()
        {
            InitializeComponent();
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            Controller.Instance.btnHomeClicked();
        }

        public void ModelChanged()
        {
            this.Visible = Controller.Instance.LijstKunstenaarsViewVisibility();

            if (this.Visible)
            {
                lblKunstenaar.Text = Controller.Instance.GetTempKunstenaar();
                lstKunstwerken.Items.Clear();
                foreach (Kunstwerk kunstwerk in Controller.Instance.GetKunstwerkenKunstenaar())
                {
                    if (kunstwerk != null)
                    {
                        lstKunstwerken.Items.Add(kunstwerk.GetNaam());
                    }
                }
            }
        }

        private void lstKunstwerken_SelectedIndexChanged(object sender, EventArgs e)
        {
            string currentItem = lstKunstwerken.SelectedItem.ToString();

            Controller.Instance.ListKunstwerkenSelected(currentItem);
        }
    }
}

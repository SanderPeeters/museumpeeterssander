﻿namespace DigitaalMuseum.View
{
    partial class LijstKunstenaarsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitel = new System.Windows.Forms.Label();
            this.lblKunstenaar = new System.Windows.Forms.Label();
            this.lstKunstwerken = new System.Windows.Forms.ListBox();
            this.btnHome = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTitel
            // 
            this.lblTitel.AutoSize = true;
            this.lblTitel.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitel.Location = new System.Drawing.Point(36, 15);
            this.lblTitel.Name = "lblTitel";
            this.lblTitel.Size = new System.Drawing.Size(671, 44);
            this.lblTitel.TabIndex = 1;
            this.lblTitel.Text = "Audiogids Museum Schone Kunsten";
            // 
            // lblKunstenaar
            // 
            this.lblKunstenaar.AutoSize = true;
            this.lblKunstenaar.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKunstenaar.Location = new System.Drawing.Point(245, 80);
            this.lblKunstenaar.Name = "lblKunstenaar";
            this.lblKunstenaar.Size = new System.Drawing.Size(250, 44);
            this.lblKunstenaar.TabIndex = 2;
            this.lblKunstenaar.Text = "[Kunstenaar]";
            // 
            // lstKunstwerken
            // 
            this.lstKunstwerken.FormattingEnabled = true;
            this.lstKunstwerken.Location = new System.Drawing.Point(44, 149);
            this.lstKunstwerken.Name = "lstKunstwerken";
            this.lstKunstwerken.Size = new System.Drawing.Size(663, 238);
            this.lstKunstwerken.TabIndex = 3;
            this.lstKunstwerken.SelectedIndexChanged += new System.EventHandler(this.lstKunstwerken_SelectedIndexChanged);
            // 
            // btnHome
            // 
            this.btnHome.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.Location = new System.Drawing.Point(525, 393);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(182, 38);
            this.btnHome.TabIndex = 5;
            this.btnHome.Text = "Home";
            this.btnHome.UseVisualStyleBackColor = true;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // LijstKunstenaarsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnHome);
            this.Controls.Add(this.lstKunstwerken);
            this.Controls.Add(this.lblKunstenaar);
            this.Controls.Add(this.lblTitel);
            this.Name = "LijstKunstenaarsView";
            this.Size = new System.Drawing.Size(750, 450);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitel;
        private System.Windows.Forms.Label lblKunstenaar;
        private System.Windows.Forms.ListBox lstKunstwerken;
        private System.Windows.Forms.Button btnHome;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DigitaalMuseum.View
{
    public partial class DefaultView : UserControl, Observer
    {
        public DefaultView()
        {
            InitializeComponent();
        }

        public void ModelChanged()
        {
            this.Visible = Controller.Instance.DefaultViewVisibility();
            txtKunstwerkNr.Text = "";
            txtKunstenaar.Text = "";
        }

        private void btnZoekOpNr_Click(object sender, EventArgs e)
        {
            Controller.Instance.btnZoekOpNrClicked(txtKunstwerkNr.Text);
        }

        private void btnZoekKunstenaar_Click(object sender, EventArgs e)
        {
            Controller.Instance.btnZoekKunstenaarClicked(txtKunstenaar.Text);
        }

    }
}

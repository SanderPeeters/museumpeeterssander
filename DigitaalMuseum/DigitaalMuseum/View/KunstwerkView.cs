﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DigitaalMuseum.View
{
    public partial class KunstwerkView : UserControl, Observer
    {
        public KunstwerkView()
        {
            InitializeComponent();
        }

        public void ModelChanged()
        {
            this.Visible = Controller.Instance.KunstwerkViewVisibility();
            if (this.Visible)
            {
                lblNaam.Text        = Controller.Instance.GetKunstwerkNaam();
                lblKunstenaar.Text  = Controller.Instance.GetKunstenaarNaam();
                lblJaar.Text        = Controller.Instance.GetKunstenaarJaar();
                rtbUitleg.Text      = Controller.Instance.GetKunstwerkUitleg();
                picFoto.Image       = Image.FromFile(@"img/" + Controller.Instance.GetKunstwerkNaam().ToLower() + ".jpg");
            }
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            Controller.Instance.btnHomeClicked();
        }
    }
}

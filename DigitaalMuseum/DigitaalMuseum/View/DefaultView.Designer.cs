﻿namespace DigitaalMuseum.View
{
    partial class DefaultView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtKunstwerkNr = new System.Windows.Forms.TextBox();
            this.btnZoekOpNr = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtKunstenaar = new System.Windows.Forms.TextBox();
            this.btnZoekKunstenaar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTitel
            // 
            this.lblTitel.AutoSize = true;
            this.lblTitel.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitel.Location = new System.Drawing.Point(41, 21);
            this.lblTitel.Name = "lblTitel";
            this.lblTitel.Size = new System.Drawing.Size(671, 44);
            this.lblTitel.TabIndex = 0;
            this.lblTitel.Text = "Audiogids Museum Schone Kunsten";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(729, 32);
            this.label1.TabIndex = 1;
            this.label1.Text = "Vul het nummer in van een kunstwerk voor extra informatie:";
            // 
            // txtKunstwerkNr
            // 
            this.txtKunstwerkNr.Font = new System.Drawing.Font("Arial Black", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKunstwerkNr.Location = new System.Drawing.Point(285, 158);
            this.txtKunstwerkNr.Name = "txtKunstwerkNr";
            this.txtKunstwerkNr.Size = new System.Drawing.Size(75, 60);
            this.txtKunstwerkNr.TabIndex = 2;
            // 
            // btnZoekOpNr
            // 
            this.btnZoekOpNr.Location = new System.Drawing.Point(390, 158);
            this.btnZoekOpNr.Name = "btnZoekOpNr";
            this.btnZoekOpNr.Size = new System.Drawing.Size(75, 60);
            this.btnZoekOpNr.TabIndex = 3;
            this.btnZoekOpNr.Text = "Zoek";
            this.btnZoekOpNr.UseVisualStyleBackColor = true;
            this.btnZoekOpNr.Click += new System.EventHandler(this.btnZoekOpNr_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(117, 254);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(525, 32);
            this.label2.TabIndex = 4;
            this.label2.Text = "Kies een kunstenaar voor extra informatie:";
            // 
            // txtKunstenaar
            // 
            this.txtKunstenaar.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKunstenaar.Location = new System.Drawing.Point(125, 365);
            this.txtKunstenaar.Name = "txtKunstenaar";
            this.txtKunstenaar.Size = new System.Drawing.Size(340, 41);
            this.txtKunstenaar.TabIndex = 5;
            // 
            // btnZoekKunstenaar
            // 
            this.btnZoekKunstenaar.Location = new System.Drawing.Point(487, 365);
            this.btnZoekKunstenaar.Name = "btnZoekKunstenaar";
            this.btnZoekKunstenaar.Size = new System.Drawing.Size(86, 41);
            this.btnZoekKunstenaar.TabIndex = 6;
            this.btnZoekKunstenaar.Text = "Zoek";
            this.btnZoekKunstenaar.UseVisualStyleBackColor = true;
            this.btnZoekKunstenaar.Click += new System.EventHandler(this.btnZoekKunstenaar_Click);
            // 
            // DefaultView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnZoekKunstenaar);
            this.Controls.Add(this.txtKunstenaar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnZoekOpNr);
            this.Controls.Add(this.txtKunstwerkNr);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblTitel);
            this.Name = "DefaultView";
            this.Size = new System.Drawing.Size(750, 450);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtKunstwerkNr;
        private System.Windows.Forms.Button btnZoekOpNr;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtKunstenaar;
        private System.Windows.Forms.Button btnZoekKunstenaar;
    }
}
